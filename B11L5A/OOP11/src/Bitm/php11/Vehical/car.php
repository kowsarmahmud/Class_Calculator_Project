<?php

namespace Bitm\php11\Vehical;

class Car {

    public $color = "red";
    public $wheel = 4;
    public $available = 2345;
    public $brand = "BMW";

    public function __construct() {
        echo 'I am new Car';
    }

    public function drive() {
        echo 'I am running day and night';
    }
   public function availableCar() {
        echo "Available Car :".$this->available;
    }
    
    public function name() {
        echo "My color is :".$this->brand;
    }

}
