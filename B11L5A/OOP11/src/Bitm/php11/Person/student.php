<?php

namespace Bitm\php11\Person;

class Student{
    
    public $name = "Rakib";
    public $age = 12;
    public $class = 5;
    public $address = "Gazipur";


    public function __construct($name=null){
        $this->name = $name;
    }
    
    public function sayHello(){
        echo "My name is::  ".$this->name = "Kobir";
    }
    
    public function nameOfclass(){
        echo "I am a student of class:: ".$this->class;
    }
    
    public function addressOfstudent(){
        echo "I read in class::  ".$this->address;
    }
    
}
