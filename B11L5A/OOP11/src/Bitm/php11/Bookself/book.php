<?php

namespace Bitm\php11\Bookself;

class Book {
    
    public $author = "Mohammad Kowsar";
    public $title = "Islamic History";
    public $numPages;
    public $available = 236;
    public $total = 12345;

    public function __construct() {
        $this->numPages = 425;
    }

    public function nameOftitle() {
        echo $this->title;
    }
   
    public function nameAuthor() {
        echo $this->author;
    }
    public function totalBook() {
        echo $this->total;
    }

}
