<?php

namespace Bitm\php11\SamrtPhone;

class Mobile {
    
    public $author = "Kowsar Mahmud";
    public $title = "Samsung";
    public $color;
    public $available = 236;
    public $total = 12345;

    public function __construct() {
        echo 'I am a new Samrt Phone Model';
    }

    public function nameOftitle() {
        echo $this->title;
    }

    public function nameAuthor() {
        echo $this->author;
    }

}
